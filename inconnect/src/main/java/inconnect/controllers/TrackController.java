package inconnect.controllers;

import inconnect.model.Track;
import inconnect.services.interfaces.TrackService;
import inconnect.utils.EncodeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;


/**
 * Created by Vasiliy on 09.04.2014.
 */
@Controller
public class TrackController {

    @Autowired
    TrackService trackService;

    @RequestMapping("/edit/{trackId}")
    public String viewTrack(Model model, @ModelAttribute Track track, @PathVariable("trackId") Integer trackId) {
        model.addAttribute("track", trackService.getTrack(trackId));

        return "edit";
    }

    @RequestMapping(value = "/play/{trackId}", method = RequestMethod.POST)
    @ResponseBody
    public String playTrack(@PathVariable("trackId") Integer trackId) {
        return EncodeUtil.encodeObject(trackService.getTrack(trackId).getData());
    }

    @RequestMapping(value = "/artwork/{trackId}", method = RequestMethod.POST)
    @ResponseBody
    public String getTrackArtwork(@PathVariable("trackId") Integer trackId) {
        return EncodeUtil.encodeObject(trackService.getTrack(trackId).getArtwork());
    }

    @RequestMapping("/tracks/{login}")
    @ResponseBody
    public Collection<Integer> getUserTracksId(@PathVariable("login") String login) {
        return trackService.getUserTracksId(login);
    }
}
