package inconnect.controllers;

import inconnect.model.Track;
import inconnect.model.User;
import inconnect.services.interfaces.TrackService;
import inconnect.services.interfaces.UserService;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.audio.mp3.MP3File;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;
import org.jaudiotagger.tag.TagException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import static inconnect.utils.FilesUtil.multipartToFile;

/**
 * Created by Vasiliy on 05.04.2014.
 */
@Controller
@RequestMapping("/upload")
public class UploadController {

    @Autowired
    UserService userService;

    @Autowired
    TrackService trackService;

    @RequestMapping("")
    public String upload() {
        return "upload";
    }

    @RequestMapping(value = "/image", method = RequestMethod.POST)
    public String uploadUserImage(@RequestParam("image") MultipartFile file, @RequestParam String login) {
        User user = userService.getUser(login);
        try {
            user.setImage(file.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        userService.updateUser(user);

        return "redirect:../settings/" + login;
    }

    @RequestMapping(value = "/track", method = RequestMethod.POST)
    public String uploadTrack(@RequestParam("track") MultipartFile file, @RequestParam("login") String login) {
        Track track = new Track();

        try {
            track.setData(file.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            MP3File audioFile = (MP3File) AudioFileIO.read(multipartToFile(file));

            if (!audioFile.hasID3v1Tag() || !audioFile.hasID3v2Tag()) {
                track.setName(file.getOriginalFilename());
            } else {
                Tag tag = audioFile.getID3v2Tag();

                track.setName(tag.getFirst(FieldKey.ARTIST) + " - " + tag.getFirst(FieldKey.TITLE));

                if (!tag.getArtworkList().isEmpty()) {
                    track.setArtwork(tag.getFirstArtwork().getBinaryData());
                }
            }
        } catch (CannotReadException | IOException | TagException | ReadOnlyFileException | InvalidAudioFrameException e) {
            e.printStackTrace();
        }

        User user = userService.getUser(login);

        track.setOwner(user);

        trackService.addTrack(track);
        userService.updateUser(user);

        return "redirect:../edit/" + track.getId();
    }
}
