package inconnect.controllers;

import inconnect.model.User;
import inconnect.services.interfaces.TrackService;
import inconnect.services.interfaces.UserService;
import inconnect.utils.EncodeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Vasiliy on 03.04.2014.
 */
@Controller

public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    TrackService trackService;

    @RequestMapping("/profile/{login}")
    public String profile(Model model, @ModelAttribute User user, @PathVariable("login") String login) {
        model.addAttribute("user", userService.getUser(login));
        model.addAttribute("tracks", trackService.getUserTracksId(login));

        return "profile";
    }


    @RequestMapping("/settings/{login}")
    public String settings(Model model, @ModelAttribute User user, @PathVariable String login) {

        model.addAttribute("user", userService.getUser(login));

        return "settings";
    }

    @RequestMapping("/update/{login}")
    public String updateUser(
            @PathVariable String login,
            String password,
            String firstName,
            String lastName,
            String city,
            String country) {
        User user = userService.getUser(login);

        user.setLogin(login);
        user.setPassword(password);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setCity(city);
        user.setCountry(country);

        userService.updateUser(user);

        return "redirect:../settings/{login}";
    }

    @RequestMapping("/image/{login}")
    @ResponseBody
    public String getUserImage(@PathVariable("login") String login) {
        return EncodeUtil.encodeObject(userService.getUser(login).getImage());
    }
}
