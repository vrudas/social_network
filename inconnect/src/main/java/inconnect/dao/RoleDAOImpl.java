package inconnect.dao;

import inconnect.dao.interfaces.RoleDAO;
import inconnect.model.Role;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by Vasiliy on 31.03.2014.
 */
@Repository
public class RoleDAOImpl implements RoleDAO {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public Role getRole(int id) {
        return (Role) getCurrentSession().load(Role.class, id);
    }
}

