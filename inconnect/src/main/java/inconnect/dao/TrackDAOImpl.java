package inconnect.dao;

import inconnect.dao.interfaces.TrackDAO;
import inconnect.model.Track;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

/**
 * Created by Vasiliy on 08.04.2014.
 */
@Repository
@Transactional
public class TrackDAOImpl implements TrackDAO {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public Track getTrack(Integer id) {
        Query query = getCurrentSession().createQuery("from Track t where t.id = :id");
        query.setParameter("id", id);

        return (Track) query.list().get(0);
    }

    @Override
    public void addTrack(Track track) {
        getCurrentSession().save(track);
    }

    @Override
    public void updateTrack(Track track) {
        getCurrentSession().update(track);
    }

    @Override
    public void deleteTrack(Integer id) {
        Track track = getTrack(id);
        if (track != null) {
            getCurrentSession().delete(track);
        }
    }

    @Override
    public Collection<Integer> getUserTracksId(String login) {
        Query query = getCurrentSession().createQuery("select id from Track t where t.owner.login = :login");
        query.setParameter("login",login);

        return query.list();
    }
}
