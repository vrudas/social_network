package inconnect.dao;

import inconnect.dao.interfaces.UserDAO;
import inconnect.model.User;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Vasiliy on 31.03.2014.
 */
@Repository
@Transactional
public class UserDAOImpl implements UserDAO {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public User getUser(String login) {
        Query query = getCurrentSession().createQuery("from User u where u.login = :login");
        query.setParameter("login", login);

        return (User) query.list().get(0);
    }

    @Override
    public void addUser(User user) {
        getCurrentSession().save(user);
    }

    @Override
    public List<User> getUsers() {
        return (List<User>) getCurrentSession().createCriteria(User.class).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
    }

    @Override
    public void deleteUser(String login) {
        User user = getUser(login);
        if (user != null) {
            getCurrentSession().delete(user);
        }
    }

    @Override
    public void updateUser(User user) {
        getCurrentSession().update(user);
    }
}
