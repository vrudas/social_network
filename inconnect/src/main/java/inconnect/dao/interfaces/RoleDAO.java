package inconnect.dao.interfaces;

import inconnect.model.Role;

/**
 * Created by Vasiliy on 31.03.2014.
 */
public interface RoleDAO {
    public Role getRole(int id);
}
