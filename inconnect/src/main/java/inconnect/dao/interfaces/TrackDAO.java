package inconnect.dao.interfaces;

import inconnect.model.Track;

import java.util.Collection;

/**
 * Created by Vasiliy on 08.04.2014.
 */
public interface TrackDAO {
    public Track getTrack(Integer id);

    public void addTrack(Track track);

    public void updateTrack(Track track);

    public void deleteTrack(Integer id);

    public Collection<Integer> getUserTracksId(String login);
}
