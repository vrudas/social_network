package inconnect.dao.interfaces;

import inconnect.model.User;

import java.util.List;

/**
 * Created by Vasiliy on 31.03.2014.
 */
public interface UserDAO {
    public User getUser(String login);

    public void addUser(User user);

    public List<User> getUsers();

    public void deleteUser(String login);

    public void updateUser(User user);
}
