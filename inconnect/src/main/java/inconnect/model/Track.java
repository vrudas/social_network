package inconnect.model;


import javax.persistence.*;

/**
 * Created by Vasiliy on 08.04.2014.
 */
@Entity
@Table(name = "track")
public class Track {
    @Id
    @GeneratedValue
    private Integer id;

    private String name;

    @Lob
    private byte[] artwork;

    @Lob
    private byte[] data;

    private String description;

    private String type;

    private String genre;

    private String tags;

    private Integer access;

//    @Transient
    @OneToOne(cascade = CascadeType.ALL)
    @JoinTable(name = "track_owners",
            joinColumns = {@JoinColumn(name = "trackId")},
            inverseJoinColumns = {@JoinColumn(name = "ownerId")}
    )
    private User owner;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getArtwork() {
        return artwork;
    }

    public void setArtwork(byte[] artwork) {
        this.artwork = artwork;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public Integer getAccess() {
        return access;
    }

    public void setAccess(Integer access) {
        this.access = access;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }
}
