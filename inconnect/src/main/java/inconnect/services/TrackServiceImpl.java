package inconnect.services;

import inconnect.dao.interfaces.TrackDAO;
import inconnect.model.Track;
import inconnect.services.interfaces.TrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * Created by Vasiliy on 08.04.2014.
 */
@Service
public class TrackServiceImpl implements TrackService{

    @Autowired
    TrackDAO trackDAO;

    @Override
    public Track getTrack(Integer id) {
        return trackDAO.getTrack(id);
    }

    @Override
    public void updateTrack(Track track) {
        trackDAO.updateTrack(track);
    }

    @Override
    public void deleteTrack(Integer id) {
        trackDAO.deleteTrack(id);
    }

    @Override
    public void addTrack(Track track) {
        trackDAO.addTrack(track);
    }

    @Override
    public Collection<Integer> getUserTracksId(String login) {
        return trackDAO.getUserTracksId(login);
    }
}
