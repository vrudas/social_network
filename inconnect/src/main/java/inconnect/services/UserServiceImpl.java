package inconnect.services;

import inconnect.dao.interfaces.UserDAO;
import inconnect.model.User;
import inconnect.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Vasiliy on 31.03.2014.
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDAO userDAO;

    public User getUser(String login) {
        return userDAO.getUser(login);
    }

    @Override
    public void updateUser(User user) {
        userDAO.updateUser(user);
    }
}
