package inconnect.services.interfaces;

import inconnect.model.Role;

/**
 * Created by Vasiliy on 31.03.2014.
 */
public interface RoleService {
    public Role getRole(int id);
}
