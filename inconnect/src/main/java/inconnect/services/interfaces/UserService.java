package inconnect.services.interfaces;

import inconnect.model.User;

/**
 * Created by Vasiliy on 31.03.2014.
 */
public interface UserService {
    public User getUser(String login);

    public void updateUser(User user);
}
