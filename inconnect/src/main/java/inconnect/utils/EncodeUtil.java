package inconnect.utils;

import java.util.Base64;

/**
 * Created by Vasiliy on 13.04.2014.
 *
 */
public class EncodeUtil {
    /**
     * Encode array of bytes to Base64 string
     * @param objectToEncode - file to encode
     * @return encoded string in Base64
     */
    public static String encodeObject(byte[] objectToEncode) {
            return Base64.getEncoder().encodeToString(objectToEncode);
    }
}
