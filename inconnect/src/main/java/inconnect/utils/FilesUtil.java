package inconnect.utils;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * Created by Vasiliy on 17.04.2014.
 */
public class FilesUtil {
    /**
     * Converts Multipart file to Java File
     *
     * @param multipart - file to convert
     * @return {@link java.io.File}
     * @throws IllegalStateException
     * @throws java.io.IOException
     */
    public static File multipartToFile(MultipartFile multipart) throws IllegalStateException, IOException {
        File tmpFile = new File(System.getProperty("java.io.tmpdir") + System.getProperty("file.separator") +
                multipart.getOriginalFilename());

        multipart.transferTo(tmpFile);

        return tmpFile;
    }
}
