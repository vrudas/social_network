<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<html>
<head>
    <title></title>
    <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/track.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/util.js"></script>

    <script>
        $(document).ready(function () {
            loadImage('artwork', '${track.id}', '/artwork/');
        });
    </script>
</head>
<body>
<sec:authorize access="!isAuthenticated()">
    <jsp:forward page="login.jsp"/>
</sec:authorize>

<sec:authorize access="isAuthenticated()">
    <img id="artwork" src="" height="512px" width="512px"/>
    <br>

    <div>
        <audio style="width: 512px" id="track_${track.id}" preload="metadata" controls="controls"
               onplay="loadUserTrack('${track.id}')" onclick="pauseAllAudio('${track.id}')">
        </audio>
    </div>
</sec:authorize>

</body>
</html>
