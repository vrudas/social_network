<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>InConnect</title>

    <!-- Bootstrap core CSS -->
    <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/bootstrap-theme.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">


</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <%--<div class="container-fluid">--%>
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">InConnect</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Link</a></li>
                <li><a href="#">Link</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                        <li class="divider"></li>
                        <li><a href="#">One more separated link</a></li>
                    </ul>
                </li>
            </ul>
            <form class="navbar-form navbar-left" role="search">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <sec:authorize access="!isAuthenticated()">
                        <a href="${pageContext.request.contextPath}/login">Log In</a>
                    </sec:authorize>
                </li>
                <li>
                    <sec:authorize access="!isAuthenticated()">
                        <form class="navbar-form navbar-right" role="form">
                            <div class="form-group">
                                <a class="btn btn-warning" href="${pageContext.request.contextPath}/connect"
                                   role="button">Sign In</a>
                            </div>
                        </form>
                    </sec:authorize>
                </li>
                <sec:authorize access="isAuthenticated()">
                    <li>
                        <form class="navbar-form navbar-right" role="form">
                            <div class="form-group">
                                <a href="${pageContext.request.contextPath}/profile/<sec:authentication
                                        property="principal.username"/>"
                                   class="btn btn-success"><sec:authentication
                                        property="principal.username"/>&nbsp;<span
                                        class="glyphicon glyphicon-user"></span></a>
                            </div>
                        </form>
                    </li>
                    <li>
                        <form class="navbar-form navbar-right" role="form">
                            <div class="form-group">
                                <p><a class="btn btn-info" href="${pageContext.request.contextPath}/upload"
                                      role="button">Upload&nbsp;<span
                                        class="glyphicon glyphicon-upload"></span></a></p>
                            </div>
                        </form>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Options&nbsp;<span
                                class="glyphicon glyphicon-cog"></span>&nbsp;</a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="/settings/<sec:authentication
                                        property="principal.username"/>">Settings</a></li>
                            <li class="divider"></li>
                            <li><a href="${pageContext.request.contextPath}/logout">Log out</a></li>
                        </ul>
                    </li>
                </sec:authorize>


            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>

<div class="container">

    <div class="tab-content">
        <h1>InConnect.com</h1>

        <p class="lead">
            InConnect - this is a service to store, listen and promote your music!.
        </p>

    </div>


</div>
<div class="container footer">
    <p>© InConnect 2014</p>
</div>
<script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.js"></script>
</body>
</html>