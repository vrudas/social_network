<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<html>
<head>
    <title></title>

    <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/json2html.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.json2html.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/track.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/util.js"></script>

    <!-- Bootstrap core CSS -->
    <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/bootstrap-theme.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">

    <script>
        $(document).ready(function () {
            loadImage('avatar', '${user.login}', '/image/');

            $.ajax({
                type: 'POST',
                url: '/tracks/' + '${user.login}',

                success: function (resp) {
                    for (var trackId in resp) {
                        loadImage('track_art_' + resp[trackId], '' + resp[trackId], '/artwork/');
                    }
                }
            });
        });
    </script>
</head>
<body>
<sec:authorize access="!isAuthenticated()">
    <jsp:forward page="login.jsp"/>
</sec:authorize>

<sec:authorize access="isAuthenticated()">
    <div class="container">
        <div class="row clearfix">
            <div class="col-md-4 column">
                <img id="avatar" alt="200x200" src="" height="200px" width="200px"/>
            </div>
            <div class="col-md-6 column">
                <c:forEach var="trackId" items="${tracks}">
                    <div class="media">
                        <a href="#" class="pull-left"><img id="track_art_${trackId}" src="" class="media-object"
                                                           alt='' height="128px" width="128px"/></a>

                        <div class="media-body">
                            <h4 class="media-heading">
                                Nested media heading
                            </h4>
                            <audio id="track_${trackId}" preload="metadata" controls="controls"
                                   onplay="loadUserTrack('${trackId}')" onclick="pauseAllAudio('${trackId}')"></audio>
                        </div>
                    </div>
                </c:forEach>
                <div id="tracks">

                </div>
            </div>
            <div class="col-md-4 column">
            </div>
        </div>
    </div>
</sec:authorize>
<%--
    <script>
        function getProfile(login) {
            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                dataType: 'json',
                url: '/profile/' + login,

                success: function (user) {
                    /*                    var image = [
                     {'key': user.encodedImage}
                     ];

                     var transform = {'tag': 'img', 'html': 'data:image;base64,\${key}'};*/

                    $('#avatar').attr('src', 'data:image;base64,' + user.encodedImage);

                }
            });
        }
    </script>
    --%>
</body>
</html>
