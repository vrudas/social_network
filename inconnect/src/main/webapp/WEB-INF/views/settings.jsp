<%--
  Created by IntelliJ IDEA.
  User: Vasiliy
  Date: 05.04.2014
  Time: 21:36
  To change this template use File | Settings | File Templates.
--%>
<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<html>
<head>
    <title></title>
    <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/json2html.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.json2html.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/util.js"></script>

    <script>
        $(document).ready(function(){
            loadImage('avatar','${user.login}','/image/');
        });
    </script>
</head>
<body>
<sec:authorize access="!isAuthenticated()">
    <jsp:forward page="login.jsp"/>
</sec:authorize>

<sec:authorize access="isAuthenticated()">

    <h3>Edit your profile settings:</h3>

    <form method="post" action="/update/${user.login}">
        <table>
            <tr>
                <td><input name="login" value="${user.login}"/></td>
            </tr>
            <tr>
                <td><input name="password" value="${user.password}"/></td>
            </tr>
            <tr>
                <td><input name="firstName" value="${user.firstName}"/></td>
            </tr>
            <tr>
                <td><input name="lastName" value="${user.lastName}"/></td>
            </tr>
            <tr>
                <td><input name="city" value="${user.city}"/></td>
            </tr>
            <tr>
                <td><input name="country" value="${user.country}"/></td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" value="Update settings"/>
                </td>
            </tr>
        </table>
        <input hidden="hidden" name="id" value="${user.id}"/>
        <input hidden="hidden" name="role" value="${user.role}"/>
    </form>

    <h3>Add profile avatar:</h3>

    <form method="post" action="/upload/image" enctype="multipart/form-data">
        <table>
            <tr>
                <td><img id="avatar" src=""
                         height="100px" width="100px"/></td>
            </tr>
            <tr>
                <td><input type="file" name="image" id="image"/></td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" value="Add avatar"/>
                </td>
            </tr>
        </table>
        <input hidden="hidden" name="login" value="${user.login}"/>
    </form>
</sec:authorize>
</body>
</html>