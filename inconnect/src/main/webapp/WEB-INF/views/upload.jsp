<%--
  Created by IntelliJ IDEA.
  User: Vasiliy
  Date: 08.04.2014
  Time: 2:15
  To change this template use File | Settings | File Templates.
--%>
<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<html>
<head>
    <title></title>
</head>
<body>
<sec:authorize access="!isAuthenticated()">
    <jsp:forward page="login.jsp"/>
</sec:authorize>

<sec:authorize access="isAuthenticated()">
    <form method="post" action="/upload/track" enctype="multipart/form-data">
        <table>
            <tr>
                <td><input type="file" name="track" id="track"/></td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" value="Add track"/>
                </td>
            </tr>
        </table>
        <input hidden="hidden" name="login" value="<sec:authentication
                                        property="principal.username"/>"/>
    </form>
</sec:authorize>
</body>
</html>
