/**
 * Created by Vasiliy on 16.04.2014.
 */
function pauseAllAudio(trackId) {
    $('audio').each(function () {
        if ($(this).get(0).getAttribute('id') != ('track_' + trackId)) {
            $(this).get(0).pause();
        }
    });
}

function loadUserTrack(id) {

    var track = $('#track_' + id);

    if (track.attr('src') == null) {
        $.ajax({
            type: 'POST',
            url: '/play/' + id,

            success: function (resp) {
                track.attr('src', 'data:audio/mpeg;base64,' + resp);
            }
        });
    }
}

function getUserTracksId(login){
    $.ajax({
        type: 'POST',
        url: '/tracks/' + login,

        success: function (resp) {

        }
    });
}