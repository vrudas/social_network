/**
 * Created by Vasiliy on 16.04.2014.
 */
function loadImage(elementId, itemId, downloadUrl) {
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        dataType: 'json',
        url: downloadUrl + itemId,

        success: function (image) {
            $('#' + elementId).attr('src', 'data:image;base64,' + image);
        }
    });
}